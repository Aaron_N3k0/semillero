package com.example.screendensity.services;

import java.util.ArrayList;

import android.app.IntentService;
import android.content.Intent;

import com.example.screendensity.models.ModelContact;
import com.example.screendensity.utilities.Util;

public class ServiceContacts extends IntentService{
	private final int CONTACTS_TO_CREATE = 10000;
	private ArrayList<ModelContact> contacts;
	
	public ServiceContacts() {
		super("ServiceContacts");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		contacts = new ArrayList<ModelContact>();
		
		for(int i = 0; i < CONTACTS_TO_CREATE; i++){
			ModelContact contact = new ModelContact();
			boolean isFavorite = i % 2 == 0;
			
			contact.setName("Nombre "+(i+1));
			contact.setAddress("Direcci�n "+(i+1));
			contact.setEmail("Email "+(i+1));
			contact.setFavorite(isFavorite);
			contact.setPhone("Tel�fono "+(i+1));
			contact.setAge(i);
			
			contacts.add(contact);
			Util.li("Contact created from service: "+(i+1));
		}
	}
}
