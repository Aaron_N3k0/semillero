package com.example.screendensity.services;

import com.example.screendensity.rest.webservices.WebServiceTransfusions;
import com.example.screendensity.rest.webservices.WebServiceTransfusions2;
import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;

public class SemilleroService extends RetrofitGsonSpiceService{

	private static final String serverUrl = "http://78.47.151.28/cruz_roja/servicios";
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		addRetrofitInterface(WebServiceTransfusions.class);
		//********************************************************
		addRetrofitInterface(WebServiceTransfusions2.class);
	}
	
	@Override
	protected String getServerUrl() {
		return serverUrl;
	}
	

}
