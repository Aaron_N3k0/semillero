package com.example.screendensity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.example.screendensity.models.ModelContact;
import com.example.screendensity.rest.models.ModelTransfusionsResponse;
import com.example.screendensity.rest.models.ModelTransfusionsResponse2;
import com.example.screendensity.rest.models.ModelTransfusionsResponse.ModelTransfusion;
import com.example.screendensity.rest.models.ModelTransfusionsResponse2.ModelTransfusion2;
import com.example.screendensity.rest.request.TransfusionsRequest;
import com.example.screendensity.rest.request.TransfusionsRequest2;
import com.example.screendensity.utilities.Util;
import com.google.gson.Gson;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

public class WebServicesActivity extends BaseActivity implements OnClickListener{
	
	String DUMMY_JSON = "{\"code\": 200,\"message\": \"Datos obtenidos\",\"contact\": {  \"name\": \"Lorena\",  \"address\": \"Fake address\",  \"phone\": \"11-22-33-44-55\", \"is_favorite\": true, \"correos\": [   \"gmail\",   \"hotmail\",   \"yahoo\" ]}}";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		 
		setContentView(R.layout.activity_web_services);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btJson:
			readJSON();
			break;
		case R.id.btGson:
			readGson();
			break;
		case R.id.btSendData:
			break;
		case R.id.btGetData:
			getData();
			break;
		}
	}
	
	private void readJSON(){
		try {
			JSONObject json = new JSONObject(DUMMY_JSON);
			ModelContact contact = new ModelContact();
			int code;
			
			code = json.getInt("code");
			
			if (code == 200){
				JSONObject jsonContact = json.getJSONObject("contact");
				JSONArray jEmails = jsonContact.getJSONArray("email");
				String[] emails = new String[jEmails.length()];
				
				contact.setName(jsonContact.getString("name"));
				contact.setAddress(jsonContact.getString("address"));
				contact.setPhone(jsonContact.getString("phone"));
				contact.setFavorite(jsonContact.getBoolean("is_favorite"));

				Util.toast(getApplicationContext(), "Nombre: "+contact.getName());
				
				for(int i = 0; i < jEmails.length(); i++){
					emails[i] = jEmails.getString(i);
					
					Util.toast(getApplicationContext(), "Email: "+emails[i]);
				}
				
			}
		} 
		catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	private void readGson(){
		Gson gson = new Gson();
		ContactResponse response = gson.fromJson(DUMMY_JSON, ContactResponse.class);
		
		if (response.getCode() == 200){
			ModelContact contact = response.getContact();
			Util.toast(getApplicationContext(), "GSON Nombre: "+contact.getName());
			
			for(String email: contact.getEmails()){
				Util.toast(getApplicationContext(), "Email: "+email);
			}
		}
	}
	
	private void getData(){
		TransfusionsRequest request = new TransfusionsRequest("2", "1");
		getSpiceManager().execute(request, new RequestListener<ModelTransfusionsResponse>() {

			@Override
			public void onRequestFailure(SpiceException arg0) {
				Util.toast(getApplicationContext(), "Fall� la petici�n");
			}

			@Override
			public void onRequestSuccess(ModelTransfusionsResponse response) {
				if (response.getCode() == 200){
					for(ModelTransfusion transfusion: response.getTransfusions()){
						Util.toast(getApplicationContext(), "Tipo de sangre: "+transfusion.getBloodType());
					}
				}
			}
		});
		
		//**********************************************************************************************
		TransfusionsRequest2 request2 = new TransfusionsRequest2("1", "1");
		
		getSpiceManager().execute(request2, new RequestListener<ModelTransfusionsResponse2>() {

			@Override
			public void onRequestFailure(SpiceException arg0) {
				Util.toast(getApplicationContext(), "Fall� la petici�n 2");
				
			}

			@Override
			public void onRequestSuccess(ModelTransfusionsResponse2 response2) {
				if(response2.getCode() == 200){					
					Util.toast(getApplicationContext(), "Parentesco: "+response2.getTransfusion().getContactPhone());
//					Util.toast(getApplicationContext(), response2.getMessage().toString());
				}
				
			}
		});
		
	}
	
	private class ContactResponse {
		int code;
		String message;
		ModelContact contact;
		
		public int getCode() {
			return code;
		}
		public String getMessage() {
			return message;
		}
		public ModelContact getContact() {
			return contact;
		}
		public void setCode(int code) {
			this.code = code;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		public void setContact(ModelContact contact) {
			this.contact = contact;
		}
		
	}
	
}
