package com.example.screendensity.rest.models;

import com.google.gson.annotations.SerializedName;

public class ModelTransfusionsRequest2 {
	@SerializedName("solicitud")
	private String requestId;
	
	@SerializedName("id_transfusion")
	private String transfusionId;
	
	
	public String getRequestId() {
		return requestId;
	}
	public String getTransfusionId() {
		return transfusionId;
	}
	
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public void setTransfusionId(String transfusionId) {
		this.transfusionId = transfusionId;
	}

}
