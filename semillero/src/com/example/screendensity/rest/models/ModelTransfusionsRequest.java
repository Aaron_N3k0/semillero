package com.example.screendensity.rest.models;

import com.google.gson.annotations.SerializedName;

public class ModelTransfusionsRequest {

	@SerializedName("solicitud")
	private String requestId;
	
	@SerializedName("id_empresa")
	private String companyId;
	
	public String getRequestId() {
		return requestId;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	
}
