package com.example.screendensity.rest.models;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class ModelTransfusionsResponse {

	@SerializedName("transfusiones")
	private ArrayList<ModelTransfusion> transfusions;
	
	@SerializedName("codigo")
	private int code;
	
	@SerializedName("mensaje")
	private String message;
	
	public ArrayList<ModelTransfusion> getTransfusions() {
		return transfusions;
	}

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public void setTransfusions(ArrayList<ModelTransfusion> transfusions) {
		this.transfusions = transfusions;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public class ModelTransfusion{
		@SerializedName("id_transfusion")
		private String transfusionId;
		
		@SerializedName("tipo_sangre")
		private String bloodType;
		
		@SerializedName("dia")
		private String day;
		
		@SerializedName("mes")
		private String month;
		
		@SerializedName("anio")
		private String year;

		public String getTransfusionId() {
			return transfusionId;
		}

		public String getBloodType() {
			return bloodType;
		}

		public String getDay() {
			return day;
		}

		public String getMonth() {
			return month;
		}

		public String getYear() {
			return year;
		}

		public void setTransfusionId(String transfusionId) {
			this.transfusionId = transfusionId;
		}

		public void setBloodType(String bloodType) {
			this.bloodType = bloodType;
		}

		public void setDay(String day) {
			this.day = day;
		}

		public void setMonth(String month) {
			this.month = month;
		}

		public void setYear(String year) {
			this.year = year;
		}
	}
}
