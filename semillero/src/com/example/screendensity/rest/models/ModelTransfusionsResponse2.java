package com.example.screendensity.rest.models;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class ModelTransfusionsResponse2 {
	
	@SerializedName("transfusion")
	private ModelTransfusion2 transfusion;

	@SerializedName("codigo")
	private int code;
	
	@SerializedName("mensaje")
	private String message;
	
	
	public ModelTransfusion2 getTransfusion() {
		return transfusion;
	}

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	
	public void setCode(int code) {
		this.code = code;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public void setTransfusion(ModelTransfusion2 transfusion) {
		this.transfusion = transfusion;
	}
	
	
	public class ModelTransfusion2{
		@SerializedName("id_transfusion")
		private String transfusionId;
		
		@SerializedName("id_usuario")
		private String userId;
		
		@SerializedName("tipo_sangre")
		private String bloodType;
		
		@SerializedName("parentesco")
		private String parent;
		
		@SerializedName("tel_contacto")
		private String contactPhone;
		
		@SerializedName("estatus")
		private String status;
		
		@SerializedName("solicitante")
		private String applicant;
		
		@SerializedName("fecha")
		private String date;
		
		@SerializedName("activo")
		private String active;
		
		@SerializedName("usuario")
		private String user;

		

		public String getTransfusionId() {
			return transfusionId;
		}

		public void setTransfusionId(String transfusionId) {
			this.transfusionId = transfusionId;
		}

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}

		public String getBloodType() {
			return bloodType;
		}

		public void setBloodType(String bloodType) {
			this.bloodType = bloodType;
		}

		public String getParent() {
			return parent;
		}

		public void setParent(String parent) {
			this.parent = parent;
		}

		public String getContactPhone() {
			return contactPhone;
		}

		public void setContactPhone(String contactPhone) {
			this.contactPhone = contactPhone;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getApplicant() {
			return applicant;
		}

		public void setApplicant(String applicant) {
			this.applicant = applicant;
		}

		public String getDate() {
			return date;
		}

		public void setDate(String date) {
			this.date = date;
		}

		public String getActive() {
			return active;
		}

		public void setActive(String active) {
			this.active = active;
		}

		public String getUser() {
			return user;
		}

		public void setUser(String user) {
			this.user = user;
		}

		
	}
}
