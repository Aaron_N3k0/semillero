package com.example.screendensity.rest.webservices;

import retrofit.http.Body;
import retrofit.http.POST;

import com.example.screendensity.rest.models.ModelTransfusionsRequest2;
import com.example.screendensity.rest.models.ModelTransfusionsResponse2;

public interface WebServiceTransfusions2 {

	@POST("/transfusion.php")
	ModelTransfusionsResponse2 getTransfusions(@Body ModelTransfusionsRequest2 body);
	
}
