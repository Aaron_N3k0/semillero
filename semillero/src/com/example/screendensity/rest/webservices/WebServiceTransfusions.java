package com.example.screendensity.rest.webservices;

import com.example.screendensity.rest.models.ModelTransfusionsRequest;
import com.example.screendensity.rest.models.ModelTransfusionsResponse;

import retrofit.http.Body;
import retrofit.http.POST;

public interface WebServiceTransfusions {

	@POST("/empresa.php")
	ModelTransfusionsResponse getTransfusions(@Body ModelTransfusionsRequest body);
	
}
