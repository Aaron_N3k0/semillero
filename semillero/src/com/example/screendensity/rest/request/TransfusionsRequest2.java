package com.example.screendensity.rest.request;

import com.example.screendensity.rest.models.ModelTransfusionsRequest2;
import com.example.screendensity.rest.models.ModelTransfusionsResponse2;
import com.example.screendensity.rest.webservices.WebServiceTransfusions2;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

public class TransfusionsRequest2 extends RetrofitSpiceRequest<ModelTransfusionsResponse2, WebServiceTransfusions2>{

	ModelTransfusionsRequest2 request;
	
	public TransfusionsRequest2(String requestId, String transfusionId) {
		super(ModelTransfusionsResponse2.class, WebServiceTransfusions2.class);
		
		request = new ModelTransfusionsRequest2();
		request.setRequestId(requestId);
		request.setTransfusionId(transfusionId);
	}

	@Override
	public ModelTransfusionsResponse2 loadDataFromNetwork() throws Exception {
		return getService().getTransfusions(request);
	}

}
