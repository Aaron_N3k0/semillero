package com.example.screendensity.rest.request;

import com.example.screendensity.rest.models.ModelTransfusionsRequest;
import com.example.screendensity.rest.models.ModelTransfusionsResponse;
import com.example.screendensity.rest.webservices.WebServiceTransfusions;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

public class TransfusionsRequest extends RetrofitSpiceRequest<ModelTransfusionsResponse, WebServiceTransfusions>{

	ModelTransfusionsRequest request;
	
	public TransfusionsRequest(String requestId, String companyId) {
		super(ModelTransfusionsResponse.class, WebServiceTransfusions.class);
		
		request = new ModelTransfusionsRequest();
		request.setRequestId(requestId);
		request.setCompanyId(companyId);
	}

	@Override
	public ModelTransfusionsResponse loadDataFromNetwork() throws Exception {
		return getService().getTransfusions(request);
	}

}
