package com.example.screendensity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class GetDataActivity extends Activity{
	
	public static final String RESULT_DATA = "result_data";
	
	private EditText etData;
	private Spinner spData;
	private Button btDone;
	private boolean requestTextFromUser;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_get_data);
		
		getExtras();
		initComponents();
		assignListeners();
	}
	
	private void getExtras(){
		Intent intent = getIntent();
		
		requestTextFromUser = intent.getBooleanExtra(RequestDataActivity.REQUEST_TEXT_FROM_USER, false);
	}
	
	private void initComponents(){
		etData = (EditText)findViewById(R.id.etData);
		spData = (Spinner)findViewById(R.id.spData);
		btDone = (Button)findViewById(R.id.btDone);
		
		if (requestTextFromUser){
			spData.setVisibility(View.GONE);
		}
		else{
			etData.setVisibility(View.GONE);
		}
	}
	
	private void assignListeners(){
		btDone.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				String result = "";
				
				if (requestTextFromUser){
					result = etData.getText().toString();
				}
				else{
					result = spData.getSelectedItem().toString();
				}
				
				intent.putExtra(GetDataActivity.RESULT_DATA, result);
				setResult(Activity.RESULT_OK, intent);
				
				finish();
			}
		});
	}
	
}
