package com.example.screendensity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.example.screendensity.fragments.FragmentA;
import com.example.screendensity.fragments.FragmentB;

public class DoubleFragmentActivity extends FragmentActivity{

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		
		setContentView(R.layout.activity_double_fragment);
		addFragments();
	}
	
	private void addFragments(){
		FragmentA fragmentA = new FragmentA();
		FragmentB fragmentB = new FragmentB();

		getSupportFragmentManager().beginTransaction().add(R.id.fragment_container_1, fragmentA).commit();
		getSupportFragmentManager().beginTransaction().add(R.id.fragment_container_2, fragmentB).commit();
		
	}
	
}
