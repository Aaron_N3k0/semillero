package com.example.screendensity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

public class StyleActivity extends Activity implements OnClickListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_style);
		
	}

	@Override
	public void onClick(View v) {
		Toast.makeText(getApplicationContext(), "Click", Toast.LENGTH_SHORT).show();
	}
	
}
