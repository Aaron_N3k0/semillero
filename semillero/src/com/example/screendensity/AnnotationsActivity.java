package com.example.screendensity;

import org.androidannotations.annotations.AfterTextChange;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.widget.TextView;

import com.example.screendensity.utilities.Util;

@EActivity(R.layout.activity_annotations)
public class AnnotationsActivity extends ActionBarActivity{

	@ViewById
	TextView tvName;
	
	@Click(R.id.bt1)
	public void doClick(){
		Util.toast(getApplicationContext(), "Click en boton 1");
	}

	@Click
	public void bt2(){
		Util.toast(getApplicationContext(), "Click en boton 2");
	}
	
	@AfterTextChange(R.id.etName)
	void afterTextChangedOnHelloTextView(Editable text, TextView hello) {
	    tvName.setText(text.toString());
	}
	
}
