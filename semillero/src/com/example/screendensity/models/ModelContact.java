package com.example.screendensity.models;

import com.google.gson.annotations.SerializedName;

public class ModelContact {

	private String id;
	private String name;
	private String phone;
	private String email;
	private String address;
	private int age;
	private boolean isFavorite;
	
	@SerializedName("correos")
	private String[] emails;
	
	public String[] getEmails() {
		return emails;
	}
	public String getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getPhone() {
		return phone;
	}
	public String getEmail() {
		return email;
	}
	public String getAddress() {
		return address;
	}
	public int getAge() {
		return age;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public boolean isFavorite() {
		return isFavorite;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public void setFavorite(boolean isFavorite) {
		this.isFavorite = isFavorite;
	}
	public void setEmails(String[] emails) {
		this.emails = emails;
	}
	
}
