package com.example.screendensity;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class FormComponentsActivity extends Activity{

	TextView tvTest;
	Button btTest;
	ToggleButton tbTest;
	CheckBox cbText;
	RadioButton rbOption1, rbOption2, rbOption3;
	Spinner spTest;
	ProgressBar pbLoader;
	ProgressBar pbHorizontal;
	SeekBar sbTest;
	EditText etPlain, etPassword, etEmail, etNumber;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_form_components);

		initComponents();
		assignListeners();

		tvTest.setText(R.string.title_form_components);
	}

	private void initComponents(){
		tvTest = (TextView)findViewById(R.id.textView1);
		btTest = (Button)findViewById(R.id.button1);
		tbTest = (ToggleButton)findViewById(R.id.toggleButton1);
		cbText = (CheckBox)findViewById(R.id.checkBox1);
		rbOption1 = (RadioButton)findViewById(R.id.radio0);
		rbOption1 = (RadioButton)findViewById(R.id.radio1);
		rbOption1 = (RadioButton)findViewById(R.id.radio2);
		spTest = (Spinner)findViewById(R.id.spinner1);
		pbLoader = (ProgressBar)findViewById(R.id.progress_circular);
		pbHorizontal = (ProgressBar)findViewById(R.id.progress_horizontal);
		sbTest = (SeekBar)findViewById(R.id.seekBar1);
		etPlain = (EditText)findViewById(R.id.etPlain);
		etPassword = (EditText)findViewById(R.id.etPassword);
		etEmail = (EditText)findViewById(R.id.etEmail);
		etNumber = (EditText)findViewById(R.id.etNumber);
	}

	private void assignListeners(){
		btTest.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String message = "Has seleccionado la opci�n ";

				if (rbOption1.isSelected()){
					message+= "1";
				}
				else if (rbOption2.isSelected()){
					message+= "2";
				}
				else if (rbOption3.isSelected()){
					message+= "3";
				}

				Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
			}
		});
		tbTest.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				String message = isChecked ? "No est� seleccionado" : "Seleccionado";

				Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
			}
		});
		etPlain.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
	}

}
