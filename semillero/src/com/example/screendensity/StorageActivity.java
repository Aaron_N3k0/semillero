package com.example.screendensity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;

import com.example.screendensity.components.Preferences;
import com.example.screendensity.db.ContactsContract;
import com.example.screendensity.db.DBHelper;
import com.example.screendensity.models.ModelContact;
import com.example.screendensity.utilities.Util;

public class StorageActivity extends ActionBarActivity implements OnClickListener{

	DBHelper dbHelper;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_storage);
		dbHelper = new DBHelper(getApplicationContext());
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btInsertSharedPreferences:
			insertSharedPreference();
			break;
		case R.id.btReadShatedPreferences:
			readSharedPreference();
			break;
		case R.id.btInsertDatabase:
			insertDatabase();
			break;
		case R.id.btReadDatabase:
			readDatabase();
			break;
		}
	}
	
	private void insertSharedPreference(){
		Preferences pref = new Preferences(getApplicationContext());
		pref.saveUserName("Romeo");
		Util.toast(getApplicationContext(), "Nombre guardado");
	}
	
	private void readSharedPreference(){
		Preferences pref = new Preferences(getApplicationContext());
		String userName = pref.getUserName();
		
		if (!TextUtils.isEmpty(userName)){
			Util.toast(getApplicationContext(), "Nombre de usuario: "+userName);
		}
		else{
			Util.toast(getApplicationContext(), "No hay nombre de usuario");
		}
	}
	
	private void insertDatabase(){
		
		ModelContact contact = new ModelContact();
		
		contact.setName("Lorena Marroquin");
		contact.setAddress("Direcci�n de Lorena");
		contact.setPhone("11-22-33-44-55");
		contact.setFavorite(true);
		
		long newId = ContactsContract.insert(dbHelper, contact);
		
		if (newId != -1){
			Util.toast(getApplicationContext(), "Contacto guardado: "+newId);
		}
		else{
			Util.toast(getApplicationContext(), "Ocurrio un problema al guardar el contacto");
		}
	}
	
	private void readDatabase(){
		ModelContact contact = ContactsContract.getSingle(dbHelper, 2);
		
		if (contact != null){
			Util.toast(getApplicationContext(), "Contacto: "+contact.getName());
		}
		else{
			Util.toast(getApplicationContext(), "No se pudo obtener el contacto");
		}
	}
	
}
