package com.example.screendensity;

import android.app.Activity;
import android.os.Bundle;
import static com.example.screendensity.utilities.Util.li;

public class LifeCycleActivity extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_life_cycle);
		
		li("onCreate");
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		li("OnStart");
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		
		li("onRestart");
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		li("onResume");
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		
		li("onPause");
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		
		li("onStop");
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		li("onDestroy");
	}
	
}
