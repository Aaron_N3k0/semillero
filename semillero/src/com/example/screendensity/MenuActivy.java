package com.example.screendensity;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;

import com.example.screendensity.utilities.Util;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

@SuppressLint("NewApi") public class MenuActivy extends Activity {

	Button btFormComponents, btStyles, btLifeCycleActivity, btRequestDataActivity, btLinearLayoutActivity, btListViewActivity, btFragments;
	Button btActionBar, btAsync, btStorage, btWebServices, btImage, btPager;
	View btTouch;
	View containerCustomView;
	View btCustomView;
	View btFade, btScale, btAnnotations;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_menu);
		
		initUIL();
		initComponents();
		assignListeners();
	}
	
	private void initUIL(){
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
		.build();
		
		ImageLoader.getInstance().init(config);
	}
	
	private void initComponents(){
		btFormComponents = (Button)findViewById(R.id.btFormComponents);
		btStyles = (Button)findViewById(R.id.btStyle);
		btLifeCycleActivity = (Button)findViewById(R.id.btLifeCycleActivity);
		btRequestDataActivity = (Button)findViewById(R.id.btRequestDataActivity);
		btLinearLayoutActivity = (Button)findViewById(R.id.btLinearLayoutActivity);
		btListViewActivity = (Button)findViewById(R.id.btListViewActivity);
		btFragments = (Button)findViewById(R.id.btFragments);
		btActionBar = (Button)findViewById(R.id.btActionBar);
		btAsync = (Button)findViewById(R.id.btAsync);
		btStorage = (Button)findViewById(R.id.btStorage);
		btWebServices = (Button)findViewById(R.id.btWebServices);
		btImage = (Button)findViewById(R.id.btImage);
		btPager = (Button)findViewById(R.id.btPager);
		btTouch = findViewById(R.id.btTouch);
		btCustomView = findViewById(R.id.btCustomView);
		containerCustomView = findViewById(R.id.containerCustomView);
		btFade = findViewById(R.id.btFade);
		btScale = findViewById(R.id.btScale);
		btAnnotations = findViewById(R.id.btAndroidAnnotations);
	}
	
	private void assignListeners(){
		
		btFormComponents.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), FormComponentsActivity.class);
				startActivity(intent);
			}
		});
		btStyles.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), StyleActivity.class);
				startActivity(intent);
			}
		});
		btLifeCycleActivity.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), LifeCycleActivity.class);
				startActivity(intent);
			}
		});
		btRequestDataActivity.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), RequestDataActivity.class);
				startActivity(intent);
			}
		});
		btLinearLayoutActivity.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), LinearLayoutContactActivity.class);
				startActivity(intent);
			}
		});
		btListViewActivity.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), ListViewContactActivity.class);
				startActivity(intent);
			}
		});
		btFragments.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), MenuFragmentsActivity.class);
				startActivity(intent);
			}
		});
		btActionBar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), MyActionBarActivity.class);
				startActivity(intent);
			}
		});

//		btAsync.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				Intent intent = new Intent(getApplicationContext(), AsyncActivity.class);
//				startActivity(intent);
//			}
//		});
		
		btStorage.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), StorageActivity.class);
				startActivity(intent);
			}
		});
		btWebServices.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), WebServicesActivity.class);
				startActivity(intent);
			}
		});
		btImage.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), ImageActivity.class);
				startActivity(intent);
			}
		});
		btPager.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), ViewPagerActivity.class);
				startActivity(intent);
			}
		});
		btAnnotations.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
//				Intent intent = new Intent(getApplicationContext(), AnnotationsActivity_.class);
//				startActivity(intent);
			}
		});
		btCustomView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				containerCustomView.setVisibility(View.GONE);
			}
		});
		btFade.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ObjectAnimator animator = ObjectAnimator.ofFloat(btFade, "alpha", 1f, 0f);
				animator.setDuration(1500);
				animator.start();
			}
		});
		btScale.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ObjectAnimator animator = ObjectAnimator.ofFloat(btScale, "scaleX", btScale.getScaleX(), btScale.getScaleX()*2);
				animator.setDuration(2000);
				animator.addListener(new AnimatorListener() {
					
					@Override
					public void onAnimationStart(Animator animation) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onAnimationRepeat(Animator animation) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onAnimationEnd(Animator animation) {
						ObjectAnimator animator = ObjectAnimator.ofFloat(btScale, "scaleX", btScale.getScaleX(), btScale.getScaleX()/2);
						animator.setDuration(2000);
						animator.start();
					}
					
					@Override
					public void onAnimationCancel(Animator animation) {
						// TODO Auto-generated method stub
						
					}
				});
				animator.start();
			}
		});
		btTouch.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				float x = event.getX();
				float y = event.getY();
				
				Util.li("X: "+x+"\nY: "+y);
				
				switch(event.getAction()){
				case MotionEvent.ACTION_CANCEL:
					Util.li("ACTION_CANCEL");
					break;
				case MotionEvent.ACTION_DOWN:
					Util.li("ACTION_DOWN");
					break;
				case MotionEvent.ACTION_MOVE:
					Util.li("ACTION_MOVE");
					break;
				case MotionEvent.ACTION_UP:
					Util.li("ACTION_UP");
					break;
				
				}
				return false;
			}
		});
	}
	
}
