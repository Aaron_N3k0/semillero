package com.example.screendensity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class FragmentLifeCycleActivity extends FragmentActivity{

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		
		setContentView(R.layout.activity_fragment_life_cycle);
	}
	
}
