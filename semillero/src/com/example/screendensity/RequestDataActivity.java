package com.example.screendensity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class RequestDataActivity extends Activity{
	
	public static final String REQUEST_TEXT_FROM_USER = "request_user_input";
	private static final int REQUEST_DATA = 0;
	
	private RadioGroup rgChoices;
	private TextView tvData;
	private Button btRequest;
	
	private boolean requestTextFromUser;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_request_data);
		
		initComponents();
		assignListeners();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if (requestCode == REQUEST_DATA && resultCode == Activity.RESULT_OK){
			if (data != null && data.hasExtra(GetDataActivity.RESULT_DATA)){
				String inputFromUser = data.getStringExtra(GetDataActivity.RESULT_DATA);
				
				if (inputFromUser != null){
					tvData.setText(inputFromUser);
				}
				else{
					tvData.setText("Error when retrieving user's value");
				}
				
			}
		}
		
	}
	
	private void initComponents(){
		requestTextFromUser = true;
		
		tvData = (TextView)findViewById(R.id.tvData);
		rgChoices = (RadioGroup)findViewById(R.id.rgChoices);
		btRequest = (Button)findViewById(R.id.btRquest);
		
		tvData.setText("");
	}
	
	private void assignListeners(){
		rgChoices.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if (checkedId == R.id.rbOption1){
					requestTextFromUser = true;
				}
				else{
					requestTextFromUser = false;
				}
				
			}
		});
		btRequest.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), GetDataActivity.class);
				
				intent.putExtra(REQUEST_TEXT_FROM_USER, requestTextFromUser);
				
				startActivityForResult(intent, REQUEST_DATA);
			}
		});
	}
	
}
