package com.example.screendensity.contacts.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.example.screendensity.R;
import com.example.screendensity.contacts.fragments.FragmentFormContact;
import com.example.screendensity.contacts.fragments.FragmentListContact;

public class ContactsActvity extends ActionBarActivity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_contacts);
		initFragments();
	}
	
	private void initFragments(){
		FragmentListContact fragmentList = new FragmentListContact();
		FragmentFormContact fragmentForm = new FragmentFormContact();
		
		fragmentForm.getOncontactCreatedListener(fragmentList);
		fragmentList.getOnContactEditableListener(fragmentForm);
		
		getSupportFragmentManager().beginTransaction().add(R.id.container_form, 
				fragmentForm).add(R.id.container_list, fragmentList).commit();
	}
}
