package com.example.screendensity.contacts.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.screendensity.R;
import com.example.screendensity.contacts.fragments.FragmentListContact.onContactEditedListener;
import com.example.screendensity.models.ModelContact;

public class FragmentFormContact extends Fragment implements onContactEditedListener{

	private View vMain;
	private EditText etName, etPhone;
	private CheckBox cbFavorite;
	private Button btSave, btnEdit;
	private int pos;
	
	//**************************************************************************
	//**************************************************************************
	private OnContactCreatedListener listener;
	
	public void getOncontactCreatedListener(OnContactCreatedListener listener){
		this.listener = listener;
	}
	//*******INTERFACE
	public interface OnContactCreatedListener {
		public void onContactCreated(ModelContact nvoContact);
		public void setContactEdited(ModelContact contactEdited, int pos);
	}
	
	@Override
	public void onContactEdited(ModelContact EditedContact, int pos) {
		this.pos = pos;
		
		etName.setText(EditedContact.getName().toString());
		etPhone.setText(EditedContact.getPhone().toString());
		btnEdit.setVisibility(View.VISIBLE);
		btSave.setVisibility(View.INVISIBLE);
	}
	//**************************************************************************
	//**************************************************************************
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		vMain = inflater.inflate(R.layout.fragment_form_contact, null);
		
		initComponents();
		assignListener();
		
		return vMain;
	}
	
	private void initComponents(){
		etName = (EditText)vMain.findViewById(R.id.etName);
		etPhone = (EditText)vMain.findViewById(R.id.etPhone);
		cbFavorite = (CheckBox)vMain.findViewById(R.id.cbFavorite);
		btSave = (Button)vMain.findViewById(R.id.btSave);
		btnEdit = (Button)vMain.findViewById(R.id.btnEdit);
	}
	
	private void assignListener(){
		btSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (isFormValid()){
					ModelContact contactForm = new ModelContact();
					
					contactForm.setName(etName.getText().toString());
					contactForm.setPhone(etPhone.getText().toString());
					contactForm.setFavorite(cbFavorite.isChecked());
					
					if (listener != null){
						listener.onContactCreated(contactForm);
					}
					
					clearForm();
				}
			}
		});
		
		btnEdit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (isFormValid()){
					ModelContact contactForm = new ModelContact();
					
					contactForm.setName(etName.getText().toString());
					contactForm.setPhone(etPhone.getText().toString());
					contactForm.setFavorite(cbFavorite.isChecked());
					
					if (listener != null){
						listener.setContactEdited(contactForm, pos);
					}
					
					clearForm();
					btnEdit.setVisibility(View.INVISIBLE);
					btSave.setVisibility(View.VISIBLE);
				}
			}
		});
	}
	
	private boolean isFormValid(){
		if (etName.length() == 0){
			toast("Ingresa el nombre del contacto");
			return false;
		}
		else if (etPhone.length() < 1){
			toast("Ingresa el tel�fono de 10 d�gitos.");
			return false;
		}
		
		return true;
	}
	
	private void clearForm(){
		etName.setText("");
		etPhone.setText("");
		cbFavorite.setChecked(false);
	}
	
	private void toast(String message){
		Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
	}
	
}
