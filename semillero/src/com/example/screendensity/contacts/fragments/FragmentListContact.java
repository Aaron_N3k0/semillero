package com.example.screendensity.contacts.fragments;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.example.screendensity.R;
import com.example.screendensity.adapters.AdapterContact;
import com.example.screendensity.contacts.fragments.FragmentFormContact.OnContactCreatedListener;
import com.example.screendensity.models.ModelContact;

public class FragmentListContact extends Fragment implements OnContactCreatedListener{

	private View vMain;
	private ListView lvContacts;
	private AdapterContact adapter;
	ArrayList<ModelContact> objetoContacto;
	
	//**********************************************************************************
	//**********************************************************************************
	onContactEditedListener editableListener;
	
	public void getOnContactEditableListener(onContactEditedListener editableListener){
		this.editableListener = editableListener;
	}
	
	//*******INTERFACE
	public interface onContactEditedListener{
		public void onContactEdited(ModelContact EditedContact, int pos);
	}
	
	@Override
	public void onContactCreated(ModelContact nvoContact) {
		adapter.addItem(nvoContact);
	}
	
	@Override
	public void setContactEdited(ModelContact contactEdited, int pos){
			objetoContacto.set(pos, contactEdited);
			adapter.notifyDataSetChanged();
	}
	//**********************************************************************************
	//**********************************************************************************
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		vMain = inflater.inflate(R.layout.fragment_contact_list, null);
		
		initComponents();
		assignListeners();
		
		return vMain;
	}	
	
	private void initComponents(){
		lvContacts = (ListView)vMain.findViewById(R.id.lvContacts);
		objetoContacto = new ArrayList<ModelContact>();
		adapter = new AdapterContact(objetoContacto, getActivity());
		lvContacts.setAdapter(adapter);
	}
	
	private void assignListeners(){
		
		lvContacts.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				final ModelContact modelo = new ModelContact();
				final int pos = position;
				
				AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
				dialog.setMessage("�Modificar?");
				dialog.setCancelable(false);
				
				dialog.setPositiveButton("Si", new DialogInterface.OnClickListener() {
					 
					  @Override
					  public void onClick(DialogInterface dialog, int which) {
						  
						  if(editableListener != null){
							  editableListener.onContactEdited(objetoContacto.get(pos), pos);
							  //adapter.notifyDataSetChanged();
						  }
					  }
					});
					dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
					 
					   @Override
					   public void onClick(DialogInterface dialog, int which) {
					      dialog.cancel();
					   }
					});
					dialog.show();
			}
		});
		
		lvContacts.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				final int pos = position;
				
				AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
				dialog.setMessage("�Borrar?");
				dialog.setCancelable(false);
				
				dialog.setPositiveButton("Si", new DialogInterface.OnClickListener() {
					 
					  @Override
					  public void onClick(DialogInterface dialog, int which) {
					      objetoContacto.remove(pos);
					      adapter.notifyDataSetChanged();
					  }
					});
					dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
					 
					   @Override
					   public void onClick(DialogInterface dialog, int which) {
					      dialog.cancel();
					   }
					});
					dialog.show();
					
				return true;
			}
		});
	}
}
