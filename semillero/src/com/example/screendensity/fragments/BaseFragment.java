package com.example.screendensity.fragments;

import android.support.v4.app.Fragment;

public abstract class BaseFragment extends Fragment{

	public abstract void setIndex(String index);
	
}
