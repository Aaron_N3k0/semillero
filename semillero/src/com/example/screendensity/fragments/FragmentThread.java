package com.example.screendensity.fragments;

import java.util.ArrayList;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.screendensity.R;
import com.example.screendensity.adapters.AdapterContact;
import com.example.screendensity.models.ModelContact;
import com.example.screendensity.utilities.Util;

public class FragmentThread extends Fragment{

	private static final int CONTACTS_TO_CREATE = 150000;
	View vMain;
	View loader;
	ListView lvContacts;
	AdapterContact adapter;
	ArrayList<ModelContact> contacts;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		vMain = inflater.inflate(R.layout.fragment_contact_list_with_loader, null);
		initComponents();
		showLoader();
		createContacts();
		
		return vMain;
	}
	
	private void initComponents(){
		contacts = new ArrayList<ModelContact>();
		
		lvContacts = (ListView)vMain.findViewById(R.id.lvContacts);
		loader = vMain.findViewById(R.id.loader);
	}
	
	private void showLoader(){
		loader.setVisibility(View.VISIBLE);
	}
	
	private void hideLoader(){
		loader.setVisibility(View.INVISIBLE);
	}
	
	private void renderContacts(){
		adapter = new AdapterContact(contacts, getActivity());
		lvContacts.setAdapter(adapter);
		hideLoader();
	}
	
	private void createContacts(){
		Thread thread = new Thread(){
			@Override
			public void run() {
				for(int i = 0; i < CONTACTS_TO_CREATE; i++){
					ModelContact contact = new ModelContact();
					boolean isFavorite = i % 2 == 0;
					
					contact.setName("Nombre "+(i+1));
					contact.setAddress("Direcci�n "+(i+1));
					contact.setEmail("Email "+(i+1));
					contact.setFavorite(isFavorite);
					contact.setPhone("Tel�fono "+(i+1));
					contact.setAge(i);
					
					contacts.add(contact);
					Util.li("Contact created from thread: "+(i+1));
				}
				
				if (getActivity() != null){
					getActivity().runOnUiThread(new Runnable() {
						
						@Override
						public void run() {
							renderContacts();
						}
					});
				}
			}
		};
		thread.start();
	}
}
