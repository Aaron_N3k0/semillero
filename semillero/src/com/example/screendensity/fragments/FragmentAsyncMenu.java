package com.example.screendensity.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.example.screendensity.R;

public class FragmentAsyncMenu extends Fragment implements OnClickListener{
	FragmentRequestedListener listener;
	View vMain;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		vMain = inflater.inflate(R.layout.fragment_async_menu, null);
		
		assignListeners();
		
		return vMain;
	}
	
	private void assignListeners(){
		vMain.findViewById(R.id.btThread).setOnClickListener(this);
		vMain.findViewById(R.id.btAsync).setOnClickListener(this);
		vMain.findViewById(R.id.btLoader).setOnClickListener(this);
		vMain.findViewById(R.id.btService).setOnClickListener(this);
	}
	
	@Override
	public void onClick(View v) {
		if (listener != null){
			switch(v.getId()){
			case R.id.btThread:
				listener.onFragmentRequested(new FragmentThread());
				break;
			case R.id.btAsync:
				listener.onFragmentRequested(new FragmentAsyncTask());
				break;
			case R.id.btLoader:
				break;
			case R.id.btService:
				listener.onFragmentRequested(new FragmentService());
				break;
			}
		}
	}
	
	public void setFragmentRequestedListener(FragmentRequestedListener listener){
		this.listener = listener;
	}
	
	public interface FragmentRequestedListener{
		public void onFragmentRequested(Fragment fragment);
	}

}
