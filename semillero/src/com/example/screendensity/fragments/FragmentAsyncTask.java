package com.example.screendensity.fragments;

import java.util.ArrayList;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.example.screendensity.R;
import com.example.screendensity.adapters.AdapterContact;
import com.example.screendensity.models.ModelContact;
import com.example.screendensity.utilities.Util;

public class FragmentAsyncTask extends Fragment{

	private static final int CONTACTS_TO_CREATE = 100;
	View vMain;
	View loader;
	ListView lvContacts;
	AdapterContact adapter;
	ArrayList<ModelContact> contacts;
	ProgressBar pbar;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		vMain = inflater.inflate(R.layout.fragment_contact_list_with_loader, null);
		initComponents();
		showLoader();
		createContacts();
		
		return vMain;
	}
	
	private void initComponents(){
		contacts = new ArrayList<ModelContact>();
		
		lvContacts = (ListView)vMain.findViewById(R.id.lvContacts);
		pbar = (ProgressBar)vMain.findViewById(R.id.progressBar1);
		loader = vMain.findViewById(R.id.loader);
		
		pbar.setProgress(0);
	}
	
	private void showLoader(){
		loader.setVisibility(View.VISIBLE);
	}
	
	private void hideLoader(){
		loader.setVisibility(View.INVISIBLE);
	}
	
	private void renderContacts(){
		adapter = new AdapterContact(contacts, getActivity());
		lvContacts.setAdapter(adapter);
		hideLoader();
	}
	
	private void createContacts(){
		AsyncWork async = new AsyncWork();
		async.execute();
	}
	
	private class AsyncWork extends AsyncTask<String, Integer, ArrayList<ModelContact>>{

		ArrayList<ModelContact> asyncContacts;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			asyncContacts = new ArrayList<ModelContact>();
		}
		
		@Override
		protected void onProgressUpdate(final Integer... values) {
			getActivity().runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					pbar.setProgress(values[0]);
				}
			});
		}
		
		@Override
		protected ArrayList<ModelContact> doInBackground(String... params) {
			for(int i = 0; i < CONTACTS_TO_CREATE; i++){
				ModelContact contact = new ModelContact();
				boolean isFavorite = i % 2 == 0;
				
				contact.setName("Nombre "+(i+1));
				contact.setAddress("Direcci�n "+(i+1));
				contact.setEmail("Email "+(i+1));
				contact.setFavorite(isFavorite);
				contact.setPhone("Tel�fono "+(i+1));
				contact.setAge(i);
				
				asyncContacts.add(contact);
				Util.li("Contact created from thread: "+(i+1));
				onProgressUpdate((i+1));
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (isCancelled()){
					break;
				}
			}
			
			if (getActivity() != null){
				getActivity().runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						renderContacts();
					}
				});
			}
			return asyncContacts;
		}
		
		@Override
		protected void onCancelled() {
			super.onCancelled();
			Log.e("Semillero", "Async cancelled");
		}
		
		@Override
		protected void onPostExecute(ArrayList<ModelContact> result) {
			contacts = result;
			renderContacts();
		}
	}
}
