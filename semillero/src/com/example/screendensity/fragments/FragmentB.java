package com.example.screendensity.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.screendensity.R;

public class FragmentB extends Fragment{
	
	TextView tvIndex;
	String index;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View layout = inflater.inflate(R.layout.fragment_color_b, null);
		
		tvIndex = (TextView)layout.findViewById(R.id.tvCounter);
		
		if (getArguments() != null && getArguments().containsKey("index")){
			index = getArguments().getString("index");
			tvIndex.setText(index);
		}
		
		return layout;
	}

	
}
