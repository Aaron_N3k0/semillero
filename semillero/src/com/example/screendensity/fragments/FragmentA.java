package com.example.screendensity.fragments;

import com.example.screendensity.R;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class FragmentA extends BaseFragment{
	
	TextView tvIndex;
	String index;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View layout = inflater.inflate(R.layout.fragment_color_a, null);
		
		tvIndex = (TextView)layout.findViewById(R.id.tvCounter);
		
		if (index != null){
			tvIndex.setText(index);
		}
		
		return layout;
	}

	@Override
	public void setIndex(String index) {
		this.index = index;
	}
	
}
