package com.example.screendensity.fragments;

import static com.example.screendensity.utilities.Util.li;

import com.example.screendensity.R;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FragmentLifeCycle extends Fragment{

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		li("On attach");
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		li("On create");
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View layout = inflater.inflate(R.layout.fragment_color_a, null);

		li("On create view");
		
		return layout;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		li("On activity created");
	}
	
	@Override
	public void onStart() {
		super.onStart();
		
		li("On start");
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		li("On resume");
	}
	
	@Override
	public void onPause() {
		super.onPause();
		
		li("On pause");
	}
	
	@Override
	public void onStop() {
		super.onStop();
		
		li("On stop");
	}
	
	@Override
	public void onDestroyView() {
		super.onDestroyView();
		
		li("On destroy view");
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		
		li("On destroy");
	}
	
	@Override
	public void onDetach() {
		super.onDetach();
		
		li("On detach");
	}
	
}
