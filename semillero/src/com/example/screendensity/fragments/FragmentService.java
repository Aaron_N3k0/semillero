package com.example.screendensity.fragments;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.screendensity.R;
import com.example.screendensity.adapters.AdapterContact;
import com.example.screendensity.models.ModelContact;
import com.example.screendensity.services.ServiceContacts;

public class FragmentService extends Fragment{
 int CONTACTS_TO_CREATE = 150000;
	View vMain;
	View loader;
	ListView lvContacts;
	AdapterContact adapter;
	ArrayList<ModelContact> contacts;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		vMain = inflater.inflate(R.layout.fragment_contact_list_with_loader, null);
		initComponents();
		showLoader();
		createContacts();
		
		return vMain;
	}
	
	private void initComponents(){
		contacts = new ArrayList<ModelContact>();
		
		lvContacts = (ListView)vMain.findViewById(R.id.lvContacts);
		loader = vMain.findViewById(R.id.loader);
	}
	
	private void showLoader(){
		loader.setVisibility(View.VISIBLE);
	}
	
	private void hideLoader(){
		loader.setVisibility(View.INVISIBLE);
	}
	
	private void renderContacts(){
		adapter = new AdapterContact(contacts, getActivity());
		lvContacts.setAdapter(adapter);
		hideLoader();
	}
	
	private void createContacts(){
		Intent intent = new Intent(getActivity(), ServiceContacts.class);
		getActivity().startService(intent);
	}
}
