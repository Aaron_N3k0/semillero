package com.example.screendensity;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.screendensity.models.ModelContact;

public class LinearLayoutContactActivity extends Activity{

	ArrayList<ModelContact> contacts;
	LinearLayout lyContactContainer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_linear_layout_contact);
		initComponents();
		createContacts();
		renderContacts();
	}
	
	private void initComponents(){
		lyContactContainer = (LinearLayout)findViewById(R.id.lyContactContainer);
	}
	
	private void createContacts(){
		contacts = new ArrayList<ModelContact>();
		
		for(int i = 0; i < 1000; i++){
			ModelContact contact = new ModelContact();
			boolean isFavorite = i % 2 == 0;
			
			contact.setName("Nombre "+(i+1));
			contact.setAddress("Direcci�n "+(i+1));
			contact.setEmail("Email "+(i+1));
			contact.setFavorite(isFavorite);
			contact.setPhone("Tel�fono "+(i+1));
			contact.setAge(i);
			
			contacts.add(contact);
		}
		
	}
	
	private void renderContacts(){
		LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
		
		for(ModelContact contact: contacts){
			View layout = inflater.inflate(R.layout.row_contact, null);
			
			TextView tvName = (TextView)layout.findViewById(R.id.tvName);
			TextView tvAddress = (TextView)layout.findViewById(R.id.tvAddress);
			TextView tvPhone = (TextView)layout.findViewById(R.id.tvPhone);
			TextView tvAge = (TextView)layout.findViewById(R.id.tvAge);
			TextView tvEmail = (TextView)layout.findViewById(R.id.tvEmail);
			ImageView ivIsFavorite = (ImageView)layout.findViewById(R.id.ivIsFavorite);
			
			tvName.setText(contact.getName());
			tvAddress.setText(contact.getAddress());
			tvPhone.setText(contact.getPhone());
			tvEmail.setText(contact.getEmail());
			tvAge.setText(""+contact.getAge());
			
			if (contact.isFavorite()){
				ivIsFavorite.setImageResource(R.drawable.star);
			}
		
			lyContactContainer.addView(layout);
		}
	}
	
}
