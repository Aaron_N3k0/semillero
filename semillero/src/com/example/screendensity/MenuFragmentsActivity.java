package com.example.screendensity;

import com.example.screendensity.contacts.activities.ContactsActvity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

public class MenuFragmentsActivity extends Activity implements OnClickListener{

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		
		setContentView(R.layout.activity_menu_fragments);
		
	}
	
	@Override
	public void onClick(View view) {
		Intent intent;
		Class<?> activityToLaunch = null;
		
		switch(view.getId()){
		case R.id.btLifeCycle:
			activityToLaunch = FragmentLifeCycleActivity.class;
			break;
		case R.id.btAddProgrammatically:
			activityToLaunch = AddFragmentProgrammaticallyActivty.class;
			break;
		case R.id.btReplace:
			activityToLaunch = ReplaceFragmentActivity.class;
			break;
		case R.id.btContactsReplace:
			activityToLaunch = ContactsActvity.class;
			break;
		case R.id.btContactsDouble:
			activityToLaunch = DoubleFragmentActivity.class;
			break;
		}
		
		if (activityToLaunch != null){
			intent = new Intent(getApplicationContext(), activityToLaunch);
			startActivity(intent);
		}
	}
	
}
