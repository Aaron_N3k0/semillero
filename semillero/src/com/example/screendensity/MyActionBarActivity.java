package com.example.screendensity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.screendensity.fragments.MyActionBarFragment;

public class MyActionBarActivity extends ActionBarActivity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_add_fragment_programmatically);
//		getSupportActionBar().setTitle("Mi t�tulo");
		MyActionBarFragment fragment = new MyActionBarFragment();
		getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, fragment).commit();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_test, menu);
		
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case R.id.action_1:
			Toast.makeText(getApplicationContext(), "accion1", Toast.LENGTH_LONG).show();
			return true;
		case R.id.action_2:
			Toast.makeText(getApplicationContext(), "accion2", Toast.LENGTH_LONG).show();
			return true;
		case R.id.action_3:
			Toast.makeText(getApplicationContext(), "accion3", Toast.LENGTH_LONG).show();
			return true;
		}
		
		return false;
	}
	
}
