package com.example.screendensity;

import com.example.screendensity.services.SemilleroService;
import com.octo.android.robospice.SpiceManager;

import android.support.v7.app.ActionBarActivity;

public abstract class BaseActivity extends ActionBarActivity{

	SpiceManager spiceManager = new SpiceManager(SemilleroService.class);
	
	@Override
	public void onStart() {
	  super.onStart();
	  spiceManager.start(this);
	}

	@Override
	public void onStop() {
	  
		if (spiceManager.isStarted()){
			spiceManager.shouldStop();
		}
		
		super.onStop();
	}
	
	protected SpiceManager getSpiceManager(){
		return spiceManager;
	}
}
