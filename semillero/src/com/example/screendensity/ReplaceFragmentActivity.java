package com.example.screendensity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;

import com.example.screendensity.fragments.FragmentA;
import com.example.screendensity.fragments.FragmentB;

public class ReplaceFragmentActivity extends FragmentActivity implements OnClickListener{

	int counter = 0;
	
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		
		setContentView(R.layout.activity_replace_fragment);
	}

	@Override
	public void onClick(View view) {
		
		switch(view.getId()){
		case R.id.btFragmentA:
			addFragmentA();
			break;
		case R.id.btFragmentB:
			AddFragmentB();
			break;
		}
		
	}
	
	private void addFragmentA(){
		FragmentA fragment = new FragmentA();
		fragment.setIndex(""+counter);
		
		//Add fragment
//		getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, fragment).addToBackStack("").commit();
		
		//Replace fragment
		getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
		
		//Aditional methods on beginTransaction
//		getSupportFragmentManager().beginTransaction().remove(fragment);
//		getSupportFragmentManager().beginTransaction().hide(fragment);
//		getSupportFragmentManager().beginTransaction().show(fragment);
		
		
		counter++;
	}
	
	private void AddFragmentB(){
		FragmentB fragment = new FragmentB();
		Bundle arguments = new Bundle();
		
		arguments.putString("index", ""+counter);
		fragment.setArguments(arguments);
		
		//Add fragment
//		getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, fragment).addToBackStack("").commit();
		
		//Replace fragment
		getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
		
		counter++;
	}
	
}
