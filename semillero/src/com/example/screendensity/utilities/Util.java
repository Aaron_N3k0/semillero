package com.example.screendensity.utilities;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.screendensity.components.BuildConfig;

public final class Util {

	private Util(){
		
	}
	
	public static void li(String message){
		Log.i(BuildConfig.appName, message);
	}
	
	public static void toast(Context context, String message){
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}
	
}
