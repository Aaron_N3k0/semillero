package com.example.screendensity;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import com.example.screendensity.adapters.AdapterContact;
import com.example.screendensity.models.ModelContact;

public class ListViewContactActivity extends Activity{

	ListView lvContacts;
	AdapterContact adapter;
	ArrayList<ModelContact> contacts;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_listview_contact);
		
		createContacts();
		initComponents();
	}
	
	private void initComponents(){
		adapter = new AdapterContact(contacts, getApplicationContext());
		
		lvContacts = (ListView)findViewById(R.id.lvContacts);
		
		lvContacts.setAdapter(adapter);
	}
	
	private void createContacts(){
		contacts = new ArrayList<ModelContact>();
		
		for(int i = 0; i < 1000; i++){
			ModelContact contact = new ModelContact();
			boolean isFavorite = i % 2 == 0;
			
			contact.setName("Nombre "+(i+1));
			contact.setAddress("Direcci�n "+(i+1));
			contact.setEmail("Email "+(i+1));
			contact.setFavorite(isFavorite);
			contact.setPhone("Tel�fono "+(i+1));
			contact.setAge(i);
			
			contacts.add(contact);
		}
		
	}
}
