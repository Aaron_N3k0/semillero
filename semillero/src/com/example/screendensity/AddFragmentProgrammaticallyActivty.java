package com.example.screendensity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.example.screendensity.fragments.FragmentSimple;

public class AddFragmentProgrammaticallyActivty extends FragmentActivity{

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		
		setContentView(R.layout.activity_add_fragment_programmatically);
		addInitialFragment();
	}
	
	private void addInitialFragment(){
		FragmentSimple fragment = new FragmentSimple();
		FragmentManager fManager = getSupportFragmentManager();
		FragmentTransaction transaction = fManager.beginTransaction();

		//Add fragment without tag
		transaction.add(R.id.fragment_container, fragment);
		//Add fragment specifying its tag
//		transaction.add(R.id.fragment_container, fragment, "tag");
		
		transaction.commit();
	}
	
}
