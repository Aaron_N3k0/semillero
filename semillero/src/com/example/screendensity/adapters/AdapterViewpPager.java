package com.example.screendensity.adapters;

import java.util.ArrayList;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class AdapterViewpPager extends FragmentPagerAdapter{

	ArrayList<Fragment> fragments;
	
	public AdapterViewpPager(ArrayList<Fragment> fragments, FragmentManager fm) {
		super(fm);
		this.fragments = fragments;
	}

	@Override
	public Fragment getItem(int position) {
		return fragments.get(position);
	}

	@Override
	public int getCount() {
		return fragments.size();
	}

}
