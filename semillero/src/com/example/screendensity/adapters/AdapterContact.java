package com.example.screendensity.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.screendensity.R;
import com.example.screendensity.models.ModelContact;

public class AdapterContact extends BaseAdapter{

	ArrayList<ModelContact> contacts;
	Context context;
	LayoutInflater inflater;
	
	public AdapterContact(ArrayList<ModelContact> contacts, Context context){
		this.contacts = contacts;
		this.context = context;
		inflater = LayoutInflater.from(context);
	}
	
	public void addItem(ModelContact contact){
		contacts.add(contact);
		notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		return contacts.size();
	}

	@Override
	public Object getItem(int position) {
		return contacts.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		ModelContact contact = contacts.get(position);
		ViewHolder holder;
		
		if (convertView == null){

			//Instanciar nueva vista
			convertView = inflater.inflate(R.layout.row_contact, null);
			holder = new ViewHolder();
			
			holder.tvName = (TextView)convertView.findViewById(R.id.tvName);
			holder.tvAddress = (TextView)convertView.findViewById(R.id.tvAddress);
			holder.tvPhone = (TextView)convertView.findViewById(R.id.tvPhone);
			holder.tvAge = (TextView)convertView.findViewById(R.id.tvAge);
			holder.tvEmail = (TextView)convertView.findViewById(R.id.tvEmail);
			holder.ivIsFavorite = (ImageView)convertView.findViewById(R.id.ivIsFavorite);
			
			convertView.setTag(holder);
		}
		else{
			//Reciclar vista
			holder = (ViewHolder) convertView.getTag();
		}
	
		holder.tvName.setText(contact.getName());
		holder.tvAddress.setText(contact.getAddress());
		holder.tvPhone.setText(contact.getPhone());
		holder.tvEmail.setText(contact.getEmail());
		holder.tvAge.setText(""+contact.getAge());
		
		if (contact.isFavorite()){
			holder.ivIsFavorite.setImageResource(R.drawable.star);
		}
		else{
			ColorDrawable color = new ColorDrawable(Color.TRANSPARENT);
			holder.ivIsFavorite.setImageDrawable(color);
		}
		
		return convertView;
	}
	
	public class ViewHolder{
		TextView tvName;
		TextView tvAddress;
		TextView tvPhone;
		TextView tvAge;
		TextView tvEmail;
		ImageView ivIsFavorite;
	}
	
}
