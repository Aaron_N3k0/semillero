//package com.example.screendensity;
//
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.support.v7.app.ActionBarActivity;
//import android.view.View;
//import android.widget.Toast;
//
//import com.example.screendensity.fragments.FragmentAsyncMenu;
//import com.example.screendensity.fragments.FragmentAsyncMenu.FragmentRequestedListener;
//
//public class AsyncActivity extends ActionBarActivity implements FragmentRequestedListener{
//	
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		
//		setContentView(R.layout.activity_async);
//		FragmentAsyncMenu fragment = (FragmentAsyncMenu) getSupportFragmentManager().findFragmentByTag("fragment_async_menu");
//		fragment.setFragmentRequestedListener(this);
//	}
//
//	@Override
//	public void onFragmentRequested(Fragment fragment) {
//		getSupportFragmentManager()
//		.beginTransaction()
//		.add(R.id.fragment_container, fragment)
//		.addToBackStack(null)
//		.commit();
//	}
//	
//}
