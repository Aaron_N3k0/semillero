package com.example.screendensity.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.example.screendensity.models.ModelContact;

public final class ContactsContract implements BaseColumns{

	public static final String TABLE_NAME = "contacts";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_ADDRESS = "address";
	public static final String COLUMN_PHONE = "phone";
	public static final String COLUMN_IS_FAVORITE = "is_favorite";
	
	private ContactsContract(){
		
	}
	
	public static long insert(DBHelper dbHelper, ModelContact contact){
		long newId;
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		ContentValues values = new ContentValues();

		values.put(ContactsContract.COLUMN_NAME, contact.getName());
		values.put(ContactsContract.COLUMN_ADDRESS, contact.getAddress());
		values.put(ContactsContract.COLUMN_PHONE, contact.getPhone());
		values.put(ContactsContract.COLUMN_IS_FAVORITE, contact.isFavorite() ? 1 : 0);
		
		newId = db.insert(ContactsContract.TABLE_NAME, null, values);
		
		return newId;
	}
	
	public static ModelContact getSingle(DBHelper dbHelper, int id){
		ModelContact contact = null;
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		Cursor cursor;
		String query = "";
		
		query+=" SELECT _id, "+COLUMN_NAME+", "+COLUMN_ADDRESS+", "+COLUMN_PHONE+", "+COLUMN_IS_FAVORITE;
		query+=" FROM "+TABLE_NAME;
		query+=" WHERE _id = "+id;
		
		cursor = db.rawQuery(query, null);
		
		if(cursor.moveToFirst()){
			contact = new ModelContact();
			
			contact.setId(cursor.getString(cursor.getColumnIndexOrThrow("_id")));
			contact.setName(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME)));
			contact.setAddress(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_ADDRESS)));
			contact.setPhone(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_PHONE)));
			contact.setFavorite(cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_IS_FAVORITE)) == 1);
		}
		
		cursor.close();
		
		return contact;
	}
	
}
