package com.example.screendensity.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper{

	private static final String DATABASE_NAME = "DBSemillero";
	private static final int DATABASE_VERSION = 1;
	
	private static final String CREATE_CONTACTS = "CREATE TABLE "+ContactsContract.TABLE_NAME + 
			"(_id INTEGER PRIMARY KEY, "
			+ContactsContract.COLUMN_NAME+" TEXT, "
			+ContactsContract.COLUMN_ADDRESS+" TEXT, "
			+ContactsContract.COLUMN_PHONE+" TEXT, "
			+ContactsContract.COLUMN_IS_FAVORITE+" INTEGER"
			+")";
	
	public DBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_CONTACTS);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS "+ContactsContract.TABLE_NAME);
		db.execSQL(CREATE_CONTACTS);
	}

}
