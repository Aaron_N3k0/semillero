package com.example.screendensity;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;

import com.example.screendensity.adapters.AdapterViewpPager;
import com.example.screendensity.fragments.FragmentA;
import com.example.screendensity.fragments.FragmentB;

public class ViewPagerActivity extends ActionBarActivity implements OnClickListener{

	ViewPager vpTest;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_viewpager);
		
		initComponents();
	}
	
	private void initComponents(){
		ArrayList<Fragment> fragments = new ArrayList<Fragment>();
		vpTest = (ViewPager)findViewById(R.id.vpTest);

		FragmentA fragmentA1 = new FragmentA();
		FragmentA fragmentA2 = new FragmentA();
		FragmentB fragmentB1 = new FragmentB();
		FragmentB fragmentB2 = new FragmentB();

		fragments.add(fragmentA1);
		fragments.add(fragmentA2);
		fragments.add(fragmentB1);
		fragments.add(fragmentB2);
		AdapterViewpPager adapter = new AdapterViewpPager(fragments, getSupportFragmentManager());
		
		vpTest.setAdapter(adapter);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btTab1:
			vpTest.setCurrentItem(0);
			break;
		case R.id.btTab2:
			vpTest.setCurrentItem(2);
			break;
		}
	}
	
}
