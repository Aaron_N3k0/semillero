package com.example.screendensity.components;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class Preferences {

	private static final String PREFERENCES_USER_SETTINGS = "USER_SETTINGS";
	private static final String KEY_USER_NAME = "user_name";
	private Context context;
	
	public Preferences(Context context){
		this.context = context;
	}
	
	public void saveUserName(String userName){
		SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_USER_SETTINGS, Context.MODE_PRIVATE);
		Editor editor = preferences.edit();
		
		editor.putString(KEY_USER_NAME, userName);
		editor.commit();
	}
	
	public String getUserName(){
		SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_USER_SETTINGS, Context.MODE_PRIVATE);
		
		return preferences.getString(KEY_USER_NAME, "");
	}
}
