package com.example.screendensity;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;

public class ImageActivity extends ActionBarActivity implements OnClickListener{

	ImageView ivPicture;
	ProgressBar pbar;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_image);
		initComponents();
		ivPicture.setVisibility(View.INVISIBLE);
		pbar.setVisibility(View.INVISIBLE);
	}
	
	private void initComponents(){
		ivPicture = (ImageView)findViewById(R.id.ivPicture);
		pbar = (ProgressBar)findViewById(R.id.progressBar1);
	}

	@Override
	public void onClick(View v) {
		pbar.setVisibility(View.VISIBLE);
		ImageLoader.getInstance().displayImage("http://www.hdwallpapers.in/walls/dreamscape-wide.jpg", ivPicture, new ImageLoadingListener() {
			
			@Override public void onLoadingStarted(String arg0, View arg1) { }
			
			@Override public void onLoadingFailed(String arg0, View arg1, FailReason arg2) { }
			
			@Override
			public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
				pbar.setVisibility(View.INVISIBLE);
				ivPicture.setVisibility(View.VISIBLE);
			}
			
			@Override public void onLoadingCancelled(String arg0, View arg1) {}
		});
	}
	
}
